import re


def increment_string(strng):
    new_strng, i = '', -1
    while abs(i) <= len(strng) and strng[i].isdigit():
        new_strng = strng[i] + new_strng
        i -= 1
    if len(new_strng) > 0:
        incremented = int(new_strng) + 1
        return strng[:-len(new_strng)] + ((len(new_strng) - len(str(incremented))) * '0') + str(incremented)
    return strng + str(1)


# def increment_string(strng):
# digit_part = re.findall(r'\d+', strng)
# if not digit_part:
#     strng = f"{strng}1"
#     print(f'res of if: {strng}')
# elif '0' in digit_part[-1]:
#     res = len(digit_part[-1]) - len(str(int(digit_part[-1])))
#     digit_new = digit_part[-1][:res] + str(int(digit_part[-1][res:]) + 1)
#     strng = strng[:-len(digit_part[-1])] + digit_new if len(digit_part[-1]) == len(digit_new) else strng[:-len(digit_part[-1])] + digit_new[1:]
# else:
#     strng = strng[:-len(digit_part[-1])] + str((int(digit_part[-1]) + 1))
# return strng
# list_string = []
# for i in range(1, len(strng) + 1):
#     if strng[-i].isdigit():
#         list_string.append(strng[-i])
#     else:
#         break
# new_string = ''.join(reversed(list_string))
# if len(new_string) > 0:
#     if '0' in new_string:
#         res = len(new_string) - len(str(int(new_string)))
#         digit_new = new_string[:res] + str(int(new_string[res:]) + 1)
#         return strng[:-len(new_string)] + digit_new if len(new_string) == len(digit_new) else strng[:-len(new_string)] + digit_new[1:]
#     else:
#         return strng[:-len(new_string)] + str((int(new_string) + 1))
# return strng + str(1)

# another solutions
# def increment_string(strng):
#     head = strng.rstrip('0123456789')
#     tail = strng[len(head):]
#     if tail == "": return strng+"1"
#     return head + str(int(tail) + 1).zfill(len(tail))

# ---------------------------------------
# import re
#
# def increment_string(s):
#     number = re.findall(r'\d+', s)
#     if number:
#         s_number = number[-1]
#         s = s.rsplit(s_number, 1)[0]
#         number = str(int(s_number) + 1)
#         return s + '0' * (len(s_number) - len(number)) + number
#     return s + '1'
# ---------------------------------------
# RECURSION!!!
# def increment_string(s):
#     if s and s[-1].isdigit():
#         return increment_string(s[:-1]) + "0" if s[-1] == "9" else s[:-1] + int(s[-1]) + 1
#     return s + "1"
# ----------------------------------------
# increment_string=f=lambda s:s and s[-1].isdigit()and(f(s[:-1])+"0",s[:-1]+str(int(s[-1])+1))[s[-1]<"9"]or s+"1"
# -------------------------------------
# from re import sub
#
# def increment_string(s):
#     return sub(r"\d*$", lambda m: "{:0{}d}".format(int(m.group(0) or 0) + 1, len(m.group(0))), s)
# ------------------------------------
# import re
# def increment_string(strng):
#     if not strng or not strng[-1].isdigit(): return strng + '1'
#     num = re.match('.*?([0-9]+)$', strng).group(1)
#     return strng[:-len(num)] + str(int(num)+1).zfill(len(num))
# ---------------------------------------

# from re import compile
#
# p = compile(r"(.*?)(\d*$)")
#
#
# def increment_string(strng):
#     s, n = p.search(strng).groups()
#     return f"{s}{int(n or 0) + 1:0{len(n)}d}"


if __name__ == '__main__':
    print(increment_string('1'))
