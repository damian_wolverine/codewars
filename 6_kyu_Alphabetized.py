import string, re

# def alphabetized(s):
    # return ''.join(j for i in string.ascii_lowercase for j in s if j.lower() == i)
    # new_s = re.findall(r'\w', s)
    # new_s = ''.join(i for i in new_s if i not in ['0','1','2','3','4','5','6','7','8','9', '_'])
    # new_string = ''
    # for i in string.ascii_lowercase:
    #     for j in new_s:
    #         if j.lower() == i:
    #             new_string += j
    # return new_string
    # s2 = ''.join(filter(str.isalpha(), s))
    # return s2

# # another solutions
def alphabetized(s):
    r = re.findall(r'[a-z]',s)
    s = sorted(r, key=str.lower)
    print (s)
    # return ''.join(sorted(re.findall(r'[a-zA-Z]',s), key=str.lower))
# # ------------------------------
# def alphabetized(s):
#     return "".join(sorted(filter(str.isalpha, s),key=str.lower))
# # ----------------------------
# def alphabetized(s):
#     return ''.join(sorted(filter(str.isalpha, s), key=str.casefold))
# # -------------------------------------
# def alphabetized(s):
#     return ''.join([x for i in range(65, 91) for x in s if x == chr(i) or x == chr(i+32)])
# # ---------------------------------
# def alphabetized(s):
#     new = ""
#     for el in s:
#         if el.isalpha():
#             new = new+el
#     return "".join(sorted(new,key=str.casefold))
# # -------------------------------
# def alphabetized(s):
#     wynik = ''
#     orda = ord('a')
#     ordz = ord('z')
#     for x in range(orda,ordz+1):
#         for znak in s:
#             if chr(x) == znak:
#                 wynik += znak
#             elif chr(x-32) == znak:
#                 wynik += znak
#     return wynik
# # ----------------------------
# def alphabetized(s):
#     return ''.join(sorted((x for x in s if x.isalpha()), key=str.lower))
# --------------------------------
# def xx_key(a):
#     if a.islower():
#         return ord(a)
#     else:
#         return ord(a)+32
# def alphabetized(s):
#     s = ''.join(s.split())
#     m = sorted(s, key=xx_key)
#     return ''.join([i for i in m if i.isalpha()])
# ---------------------------------
# def alphabetized(s):
#     return ''.join(sorted( ''.join([x for x in s if x.isalpha()]) , key=lambda x: x.lower()))
# ----------------------------------
# def alphabetized(s):
#     # print(sorted(s, key=lambda ch: ch.lower()))
#     return ''.join(ch for ch in sorted(s, key=lambda ch: ch.lower()) if ch.isalpha())

# def alphabetized(s):
#     return ', '.join(sorted(filter(str.isalpha(), s), key=str.lower()))

if __name__ == '__main__':
    s = "A b aiAAB a ; )"
    fruits = ['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']
    # s = " a b c d e f A B C D E F"
    # s = "The Holy Bible"
    # "BbeehHilloTy"
    # "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ"
    # res = alphabetized(s)
    # for i in res:
    #     print(i)
    print(alphabetized(s))