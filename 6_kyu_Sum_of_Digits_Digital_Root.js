// my solution
function sum_of_digits(n) {
  if (n < 10) {
    return n
  }
  var string = 0;
  var n_str = n.toString()
  for (var i = 0; i < n_str.length; i++){
    // console.log(n_str[i])
    string += Number(n_str[i]);
  }
  // console.log("string val: " + string);
  return sum_of_digits(string);
}

// another solutions
function digital_root(n) {
  return (n - 1) % 9 + 1;
}
//-------------------------------------
function digital_root(n) {
  if (n < 10) return n;

  return digital_root(
    n.toString().split('').reduce(function(acc, d) { return acc + +d; }, 0));
}
//--------------------------------------
function digital_root(n) {
  if (n < 10)
    return n;

  for (var sum = 0, i = 0, n = String(n); i < n.length; i++)
    sum += Number(n[i]);

  return digital_root(sum);
}
//-----------------------------------------
function digital_root(n){
    n = eval(n.toString().split('').join('+'));

    if (n > 9) {
        return digital_root(n);
    }

    return n;
}
//---------------------------------------
function digital_root(n) {
  if (n < 10) return n
  return digital_root(n % 10 + digital_root(Math.floor(n / 10)))
}
// ------------------------------------------
function digital_root(n) {
  while (n > 9) { n = (''+n).split('').reduce(function(s,d) {return +s + +d;}); }
  return n;
}
//------------------------------------------
function digital_root(n) {
  return n < 10 ? n : digital_root(String(n).split('').reduce((s,v)=>Number(s)+Number(v)));
}
//----------------------------------------
function digital_root(n) {
  let finalSum = 10, tempSum = 0;
  while (finalSum > 9) {
    while (n > 9) {
      tempSum += n%10;
      n = parseInt(n/10);
    }
    tempSum += n;
    finalSum = tempSum;
    n = finalSum;
    tempSum = 0;
  }
  return finalSum;
}
//-------------------------------------
function digital_root(n) {
  var s = 0;
  while (n) {
    s+=n%10;
    n=Math.floor(n/10);
  }
  return s < 10 ? s : digital_root(s);
}
//------------------------------------


console.log(sum_of_digits(942))

