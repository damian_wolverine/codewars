from functools import reduce

# my solutions
# def digital_root(n):
#     return n if n < 10 else digital_root(sum(int(i) for i in str(n)))
    # num_str = str(n)
    # while len(num_str) > 1:
    #     res_sum = sum(int(i) for i in num_str)
    #     num_str = str(res_sum)
    # return int(num_str)
    # --------------------------------------
    # if n < 10:
    #     return n
    # else:
    #     # number = sum(int(i) for i in str(n))
    #     return digital_root(sum(int(i) for i in str(n)))
    # ---------------------------------------

# # another solutions
# def digital_root(n):
#     return n if n < 10 else digital_root(sum(map(int,str(n))))
# # --------------------------------
def digital_root(n):
    res = n%9 or n and 9
    return res
    # return n%9 or n and 9
# # ----------------------------------
# def digital_root(n):
#     # ...
#     while n>9:
#         n=sum(map(int,str(n)))
#     return n
# # -----------------------------------
# def digital_root(n):
#     while n>10:
#         n = sum([int(i) for i in str(n)])
#     # return n
# # ----------------------------------
# def digital_root(n):
#     while (n > 9):
#         n = reduce(lambda a, b: int(a) + int(b), list(str(n)))
#     return n
# # ---------------------------------

if __name__ == '__main__':
    result = digital_root(16)
    if result == 7:
        print(result)
    else:
        print(False)
    # print(result)