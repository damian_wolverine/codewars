# my solution
# def tribonacci(signature, n):
#     a = signature[0]
#     b = signature[1]
#     c = signature[2]
#     d = {0:[], 1:[a], 2:[a, b]}
#     if n in d.keys():
#         return d[n]
#     else:
#         ls = [a, b, c]
#         for i in range(3, n):
#             d = a + b + c
#             a, b, c = b, c, d
#             ls.append(d)
#         return ls

# another solutions
# def tribonacci(signature, n):
#     a, b, c = signature
#
#     result = []
#     while n > 0:
#         result.append(a)
#         a, b, c = b, c, (a + b + c)
#         n -= 1
#
#     return result


# def tribonacci(signature, n):
#     for i in range(3, n):
#         signature.append(signature[i-3] + signature[i-2] + signature[i-1])
#     return signature[:n]


# def tribonacci(signature,n):
#     for i in range(n):
#         signature.append(sum(signature[-3:]))
#     return signature[:n]


# def tribonacci(signature,n):
#     return signature[:1] + tribonacci(signature[1:] + [sum(signature)], n - 1) if n > 0 else []


def tribonacci(signature,n, depth = 1):
    if n <= 0:
        print('\t' * depth, 'Returning []')
        return []
    else:
        print('\t' * depth, 'Recursively calling factorial(', signature, n - 1, ')')
        res = signature[:1] + tribonacci(signature[1:] + [sum(signature)], n - 1, depth + 1)
        print('\t' * depth, 'Returning:', res)
        return res


# def tribonacci(signature, n):
#     if len(signature) < n:
#         return tribonacci(signature + [sum(signature[-3:])], n)
#     return signature[:n]


# def tribonacci(sig,n):
#     while len(sig) < n:
#         sig.append(sum(sig[-3:]))
#     return sig[:n]


if __name__ == '__main__':
    print(tribonacci([2, 4, 1], 4))
