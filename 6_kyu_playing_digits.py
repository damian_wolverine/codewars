# my solution
def dig_pow(n, p):
    res = sum([int(i)**x for x, i in enumerate(str(n), p)])
    return res / n if res % n == 0 else -1

# another solutions
def dig_pow(n, p):
    k, fail = divmod(sum(int(d)**(p + i) for i, d in enumerate(str(n))), n)
    return -1 if fail else k
# -----------------------------

def dig_pow(n, p):
  t = sum( int(d) ** (p+i) for i, d in enumerate(str(n)) )
  return t//n if t%n==0 else -1


if __name__ == '__main__':
    pass