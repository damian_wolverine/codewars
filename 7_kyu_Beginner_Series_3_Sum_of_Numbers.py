# another resolves

# def get_sum(a,b):
#     return sum(range(min(a,b), max(a,b)+1))
#
# def get_sum(a, b):
#     return (a + b) * (abs(a - b) + 1) // 2
#
#
def get_sum(a, b):
    if a > b: a, b = b, a
    return sum(range(a, b + 1))
#
#
# def get_sum(a, b):
#     high = max(a, b)
#     low = min(a, b)
#     return sum(range(low, high + 1))
#
#
# def get_sum(a, b):
#     a, b = sorted((a, b))
#     return sum(range(a, b + 1))
#
#
# def get_sum(a, b):
#     am = min(a, b)
#     bm = max(a, b)
#     return (bm - am + 1) * (am + bm) / 2


if __name__ == '__main__':
    print(get_sum(-20, 10))