#!/usr/bin/python3

"""
7 kyu Find all occurrences of an element in an array
Given an array (a list in Python) of integers and an integer n, find all occurrences of n in the given array and return another array containing all the index positions of n in the given array.
If n is not in the given array, return an empty array [].
Assume that n and all values in the given array will always be integers.
Example:
find_all([6, 9, 3, 4, 3, 82, 11], 3)
> [2, 4]
"""

def find_all(array, n):
    return [i[0] for i in enumerate(array) if i[1] == n]

"""
# mokahaiku
import numpy as np

def find_all(array, n):
    return list(np.where(np.array(array) == n)[0])

# 2 warriors
def find_all(array, n):
    return [index for index, item in enumerate(array) if item == n]

"""



if __name__ == "__main__":
    print(find_all([6, 9, 3, 4, 3, 82, 11], 3))
