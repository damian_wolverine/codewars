import ast
from ast import parse


# another solutions
# def calc_pol(pol_str, x = None):
#     try:
#         res = str(eval(pol_str))
#         print(res, (res=='1'))
#         return 'Result = ' + res + (res=='0') * ", so {} is a root of {}".format(x,pol_str)
#     except:
#         return 'There is no value for x'
# -------------------------------------

def calc_pol(pol_str, x = None):
    return "There is no value for x" if x == None else "Result = " + str(eval(pol_str)) + ("" if eval(pol_str) != 0 else ", so {} is a root of {}".format(x, pol_str))
# -------------------------------------

# def calc_pol(poly, x=None):
#     if x is None:
#         return "There is no value for x"
#     result = eval(compile(parse(poly, mode="eval"), filename="<string>", mode="eval"))
#     return f"Result = {result}{'' if result else f', so {x} is a root of {poly}'}"

if __name__ == '__main__':
    print(calc_pol("2*x**2 + 3*x - 44", 4))