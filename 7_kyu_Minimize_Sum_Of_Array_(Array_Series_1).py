# import sort as sort


def min_sum(arr):
    # new_arr = sorted(arr)
    # print(new_arr)
    high_num = [i for i in reversed(sorted(arr)[len(arr)//2:])]
    low_num = [i for i in sorted(arr)[:len(arr)//2]]
    print(high_num, low_num)
    result = 0
    for a,b in zip(high_num, low_num):
        result += a * b
    return result


if __name__ == '__main__':
    print(min_sum([12,6,10,26,3,24]))
