# 7 kyu Simple Prime Number Generator

# Your task here is to generate a list of prime numbers, starting from 2. One way of doing this is using python's
# generators. In case you choose to use generators, notice that the generator object should contain all the generated
# prime numbers, from 2 to an upper limit (included) provided as the argument to the function. If the input limit
# is less than 2 (including negatives), it should return an empty list. Each iteration of the generator will yield
# one prime number. See this link for reference. The generator object will be converted to a list outside the code,
# within the test cases. There will be no check if you are using generators or lists, so use the one you feel more
# confortable with.

# moje rozwiazania + z whilem
from math import sqrt


# def isPrime(n):
#     for i in range(2, n):
#         if n % i == 0:
#             return False
#     return True
#
#
# def generate_primes(x):
#     if x < 2:
#         return []
#     new_list = []
#     for i in range(2, x + 1):
#         if isPrime(i):
#             new_list.append(i)
#     return new_list

# z sqrt moje złożoność 0(n^3/2)


# def isPrime(n):
#     sqrt_p = int(sqrt(n))
#     print(sqrt_p)
#     for i in range(2, sqrt_p + 1):  # sqrt musi być mniejszy lub rowny stąd + 1 załatwianam sprawe
#         if n % i == 0:
#             return False
#     return True
#
#
# def generate_primes(x):
#     if x < 2:
#         return []
#     new_list = []
#     for i in range(2, x + 1):  # bez + 1 gdy wejscie x = 2 nie wykouje nam liczenia i wywala błąd
#         if isPrime(i):
#             new_list.append(i)
#     return new_list


# z geeks for geeks sqrt ale z whilem
# function to check if the number is
# prime or not
def isPrime(n):
    # Corner cases
    if n <= 1:
        return False
    if n <= 3:
        return True
    # This is checked so that we can skip
    # middle five numbers in below loop
    if (n % 2 == 0 or n % 3 == 0):
        return False
    # sprawdzamy co 6
    i = 5
    while (i * i <= n):
        if (n % i == 0 or n % (i + 2) == 0):
            return False
        i = i + 6
    return True


# print all prime numbers
# less than equal to N
def generate_primes(n):
    new_list = []
    for i in range(2, n + 1):
        if isPrime(i):
            new_list.append(i)
    return new_list


if __name__ == '__main__':
    print(generate_primes(25))
