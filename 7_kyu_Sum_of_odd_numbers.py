# my good resolution and efficient
# def row_sum_odd_numbers(n):
#     return n**3



# my good resolution but not efficient in www page
# def row_sum_odd_numbers(n):
#     nn = sum(range(1, n + 1))
#
#     first_list = []
#     for i in range(1, nn + 1):
#         while first_list.count(i) < i:
#             first_list.append(i)
#
#     second_list = []
#     counter = 1
#     while nn > 0:
#         second_list.append(counter)
#         counter += 2
#         nn -= 1
#
#     zip_list = zip(first_list, second_list)
#     d = dict.fromkeys(range(1, n + 1), 0)
#     for i, f in zip_list:
#         if i in d.keys():
#             d[i] += f
#         else:
#             pass
#     return d[n]

# bad resolution problem with dictionary
# def row_sum_odd_numbers(n):
#     nn = sum(range(1, n + 1))
#     d = dict.fromkeys(range(1, n + 1), [])
#     keyList = {value: key + 1 for key, value in enumerate(sorted(d.keys()))}
#     # print(d, keyList)
#     a = 1
#     b = 1
#     for i in range(nn):
#         # a += 1
#         print(a)
#         if nn < 3:
#             d[1].append(a)
#             a += 1
#             nn -= 1
#         elif nn < 5:
#             d[2].append(a)
#             a += 1
#             nn -= 1
#         else:
#             d[3].append(a)
#             a += 1
#             nn -= 1
#     print(d)


# def row_sum_odd_numbers(n):
#     nn = sum(range(1, n + 1))
#     x = 1
#     for i in range(nn - 1):
#         x += 2
#     print(x)

# -----------------------------
# def row_sum_odd_numbers(n):
#     prev = 1
#     ll = [y*[0] for y in range(1, n+1)]
#     for x in ll:
#         for i in range(len(x)):
#             x[i] = prev
#             prev += 2
#     return sum(ll[-1])
# --------------------------------
def row_sum_odd_numbers(n):
    a = [y*[False] for y in range(1, n+1)]
    odds = iter(list(range(1, 10**6, 2)))
    return sum([[next(odds) for c in x if c is False] for x in a][-1])
# --------------------------------



if __name__ == '__main__':
    print(row_sum_odd_numbers(4))
