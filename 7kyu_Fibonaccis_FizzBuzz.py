# # my solution
# def fibs_fizz_buzz(n):
#     ls, d = [1, 1], {}
#     for i in range(2, n):
#         ls.append(sum([ls[-2], ls[-1]]))
#         if not ls[i] % 3 and ls[i] % 5 == 0:
#             d[ls[i]] = 'FizzBuzz'
#         elif not ls[i] % 3:
#             d[ls[i]] = 'Fizz'
#         elif not ls[i] % 5:
#             d[ls[i]] = 'Buzz'
#     for i in range(len(ls)):
#         if ls[i] in d.keys():
#             ls[i] = d[ls[i]]
#     return ls[:n]

# def fibs_fizz_buzz(n):
#     a, b, ls = 0, 1, []
#     for i in range(n):
#         a, b = b, a + b
#         if not a % 15:
#             ls.append("FizzBuzz")
#         elif not a % 5:
#             ls.append("Buzz")
#         elif not a % 3:
#             ls.append("Fizz")
#         else:
#             ls.append(a)
#     return ls
# another solution
def fibs_fizz_buzz(n):
    a, b, out = 0, 1, []

    for i in range(n):
        s = "Fizz" * (b % 3 == 0) + "Buzz" * (b % 5 == 0)
        out.append(s if s else b)
        a, b = b, a + b

    return out
# # ----------------------
# def fibs_fizz_buzz(n):
#     a, b, f = 1, 1, []
#     for _ in range(n):
#         s = ""
#         if not a % 3: s += "Fizz"
#         if not a % 5: s += "Buzz"
#         f.append(s or a)
#         a, b = b, a + b
#     return f
# # ----------------------
# from itertools import islice
#
# def fib():
#     a, b = 0, 1
#     while True:
#         a, b = b, a + b
#         yield a
#
# def fibs_fizz_buzz(n):
#     return ['Fizz' * (not i % 3) + 'Buzz' * (not i % 5) or i for i in islice(fib(), n)]
# # ---------------------
# def fibs_fizz_buzz(n):
#     r, a, b = [], 0, 1
#     for i in range(n):
#         r.append("Fizz" * (not b % 3) + "Buzz" * (not b % 5) or b)
#         a, b = b, a + b
#     return r
# # --------------------
# def fizz_buzz(n):
#     return "FizzBuzz" if not n % 15 else "Buzz" if not n % 5 else "Fizz" if not n % 3 else n
#
#
# def fibs_generator(n):
#     a, b = 1, 1
#     for _ in range(n):
#         yield a
#         a, b = b, a + b
#
#
# def fibs_fizz_buzz(n):
#     return [fizz_buzz(x) for x in fibs_generator(n)]
# # ---------------------------
# def ffb(n):
#     a, b = 1, 1
#     for i in range(n):
#         if a % 15 == 0:
#             yield 'FizzBuzz'
#         elif a % 3 == 0:
#             yield 'Fizz'
#         elif a % 5 == 0:
#             yield 'Buzz'
#         else:
#             yield a
#         a, b = b, a + b
#
#
# def fibs_fizz_buzz(n):
#     return list(ffb(n))
# # --------------------------
# # RECURSION!
# __import__("sys").setrecursionlimit(2600)
#
# from functools import lru_cache
# fib = lru_cache(maxsize=None)(lambda n: n if n<2 else fib(n-1)+fib(n-2))
# fizz_buzz = lru_cache(maxsize=None)(lambda n: "FizzBuzz" if not n%15 else "Buzz" if not n%5 else "Fizz" if not n%3 else n)
# fibs_fizz_buzz = lru_cache(maxsize=None)(lambda n: ([] if n==1 else fibs_fizz_buzz(n-1)) + [fizz_buzz(fib(n))])
# # -------------------------
# def fib(n):
#     f = [1,1]
#     while len(f) < n:
#         f.append(f[-2]+f[-1])
#     return f[:n]
#
#
# def fizz_buzzify(l):
#     return ["Fizz"*(n%3==0)+"Buzz"*(n%5==0) or n for n in l]
#
#
# def fibs_fizz_buzz(n):
#     return fizz_buzzify(fib(n))
# # -----------------------------
# def fibs_fizz_buzz(n):
#     a = 0
#     b = 1
#     lst = []
#     for i in range(n):
#         a, b = b, a + b
#         if a % 3 == 0 and a % 5 == 0:
#             lst.append("FizzBuzz")
#         elif a % 5 == 0:
#             lst.append("Buzz")
#         elif a % 3 == 0:
#             lst.append("Fizz")
#         else:
#             lst.append(a)
#     return lst


if __name__ == '__main__':
    print(fibs_fizz_buzz(16))