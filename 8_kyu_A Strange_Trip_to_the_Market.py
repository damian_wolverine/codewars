def is_lock_ness_monster(string):
    return 'tree fiddy' in string or '3.50' in string or 'three fifty' in string

# another solutions
def is_lock_ness_monster(s):
    return any(i in s for i in ('tree fiddy', 'three fifty', '3.50'))
# -----------------------------------
import re

def is_lock_ness_monster(s):
    return bool(re.search(r"3\.50|tree fiddy|three fifty", s))
# -------------------------------------
def is_lock_ness_monster(string):
    return any(1 for x in ["3.50","tree fiddy","three fifty"] if x in string)


if __name__ == '__main__':
    print(is_lock_ness_monster("Your girlscout cookies are ready to ship. Your total comes to tree fiddy"))
    print(is_lock_ness_monster("Howdy Pardner. Name's Pete Lexington. I reckon you're the kinda stiff who carries about tree fiddy?"))
    print(is_lock_ness_monster("Yo, I heard you were on the lookout for Nessie. Let me know if you need assistance."))