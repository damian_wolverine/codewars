

# my resolution
def expression_matter(a, b, c):
    res1 = a * (b + c)
    res2 = a * b * c
    res3 = a + b * c
    res4 = (a + b) * c
    res5 = a * b + c
    res6 = a + b + c
    return max([res1, res2, res3, res4, res5, res6])


# def expression_matter(a, b, c):
#     return max(a * b * c, a + b + c, (a + b) * c, a * (b + c))


# def expression_matter(a, b, c):
#   return max(max(eval(f'{a}{op1}({b}{op2}{c})'), eval(f'({a}{op1}{b}){op2}{c}')) for op1 in '+*' for op2 in '+*')


# def expression_matter(a, b, c):
#     if a == 1: b += 1
#     if c == 1: b += 1
#     if b == 1:
#         if a < c:
#             a += 1
#         else:
#             c += 1
#     print(a, b, c)
#     return a * b * c


expression_matter = lambda a, b, c: max(a + b + c, a * (b + c), (a + b) * c, a * b * c)

if __name__ == '__main__':
    print(expression_matter(1, 10, 3))
