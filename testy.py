import re

# Example strings
text1 = "Python is awesome"
text2 = "Python is awesome like crazy"

# Full matching
mat = re.fullmatch("Python is awesome", text1)
print(mat)
mat = re.fullmatch("Python is awesome+", text2)
print(mat)

# Regular matching
mat = re.search("^Python is awesome$", text1)
print(mat)
mat = re.search("^Python is awesome$", text2)
print(mat)

str = "The rain in Spain"
x = re.search("S\W+", str)
print(x)    