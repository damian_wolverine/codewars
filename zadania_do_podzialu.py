import re

# def solve(s):
#     cnt = ''
#     for i in s[::-1]:
#         if i == ' ':
#             cnt += ' '
#         else:
#             break
#     len_in_str = [len(i) for i in s.split()]
#     new_string = s[::-1].replace(" ", "")
#     new_s = []
#     for i in range(len(len_in_str)):
#         new_s.append(new_string[:len_in_str[i]])
#     new_string = new_string[len_in_str[i]:]
#     return ' '.join(new_s).lower() + cnt


# -------------------------
# indexy ze spacjami
# berzo
# def solve(s):
#     space_index = [i for i in range(len(s)) if s[i] == " "]  # find index of saces
#     s = ''.join(s.split())  # remove spaces
#     s = s[::-1]  # reverse the string
#     for i in space_index:  # add spaces again to exactly same place before
#         s = s[:i] + " " + s[i:]
#     return s


# def solve(s):
#     list_of_index = [index for index, value in enumerate(s) if value == ' ']
#     new_string = s[::-1]
#     print (new_string)
#     new_s = ''
#     for i in range(len(new_string)):
#         if i in list_of_index:
#             new_s += ' ' + new_string[i]
#         else:
#             new_s += new_string[i]
#     return new_s

# ------------------
# Chris_Rands
def solve(s):
    it = reversed(s.replace(' ', ''))
    return ''.join(c if c == ' ' else next(it) for c in s)


# ------------------
# MaxSize
# def solve(s):
#     ans = list(s.replace(' ', '')[::-1])
#     for i in [i for i, item in enumerate(s) if item == ' ']:
#         ans.insert(i, ' ')
#     return ''.join(ans)
#
#
# def rever(st):
#     new_str = ''
#     for char in range(len(st) - 1, -1, -1):
#         new_str += st[char]
#     return new_str


# def draw_stairs(n):
# #     rozwiazanie dla for
# #     stairs = ''
# #     for i in range(1, n+1):
# #         if i-1 == n-1:
# #             stairs += 'I'
# #         else:
# #             stairs += 'I\n{}'.format(i*' ')
# #     return stairs
#     stairs = ''
#     cnt = 1
#     while cnt < n:
#         stairs += 'I\n{}'.format(cnt * ' ')
#         cnt +=1
#     return stairs + 'I'
# # -----------------------
# # dipterix
# def draw_stairs(n):
#     return 'I\n'.join(map(' '.__mul__, range(n))) + 'I'
# # ----------------------
# # 46 more warriors
# def draw_stairs(n):
#     return '\n'.join(' '*i+'I' for i in range(n))
# # ----------------------
# falsetru
# def draw_stairs(n):
#     return '\n'.join('{:>{}}'.format('I', i + 1) for i in range(n))


# -----------------------
# VALIDATE_PIN
#  TheBMachine
# validate_pin = lambda pin: len(pin) in (4, 6) and pin.isdigit()
# ---------------------------
# 33 more warriors
# import re
# def validate_pin(pin):
#     return bool(re.match(r'^(\d{4}|\d{6})$',pin))
# --------------------------
# def validate_pin(pin):
#     return len(pin) in [4, 6] and pin.isnumeric()  # dziala na pythonie 3.6
#
#
# # SUM POSITIVES
# # 2 warriors
# def positive_sum(arr):
#     return sum(max(i, 0) for i in arr)
# # # ADDITIONAL DESCRIBE OF MAX() FUNCTION
# # # using max(arg1, arg2, *args)
# # print('Maximum is:', max(1, 3, 2, 5, 4))
# # # using max(iterable)
# # num = [1, 3, 2, 8, 5, 10, 6]
# # print('Maximum is:', max(num))
# # Maximum is: 5
# # Maximum is: 10
# ---------------------
# # 200 warriors
# def positive_sum(arr):
#     return sum(filter(lambda x: x > 0,arr))
# # ---------------
# def positive_sum(arr):
#     return sum(i for i in arr if i > 0)


def square_digits(num):
    return int(''.join(str(int(i) ** 2) for i in str(num)))


# -------------------
# 65 warriors
def square_digits(num):
    return int(''.join(map(lambda x: str(int(x) ** 2), str(num))))


# ----------------------
# 10 warriors
def square_digits(num):
    ret = ""
    for x in str(num):
        ret += str(int(x) ** 2)
    return int(ret)


# ----------------------

def remove_smallest(numbers):
    # dalej nowa nazwa listy odnosi sie do tego samego miejsca w pamieci
    # modyfikujac new_numbers modyfikujemy tak samo numbers
    # new_numbers = numbers
    # if len(new_numbers)>0:
    #     new_numbers.remove(min(new_numbers))
    #     return new_numbers
    # return []
    new_numbers = numbers
    if len(new_numbers) > 0:
        new_numbers.remove(min(new_numbers))
        return new_numbers
    return []


# ---------------
def remove_smallest(x):
    return [x[i] for i in range(len(x)) if i != x.index(min(x))]


# ----------------
def remove_smallest(ns):
    nss = ns.copy()
    nss.remove(min(ns)) if nss else None
    return nss


# --------------------
def remove_smallest(numbers):
    return [x for i, x in enumerate(numbers) if i != min(range(len(numbers)), key=numbers.__getitem__)]


# ------------------------
def remove_smallest(numbers):
    t = numbers[:]  # tak tez tworzymy nowa liste
    if numbers:
        t.remove(sorted(t)[0])  # sorted nie modyfikuje listy tylko tymczasowo ja sortuje
        return t
    else:
        return t


# --------------------------
def remove_smallest(numbers):
    return numbers[0:numbers.index(min(numbers))] + numbers[numbers.index(min(numbers)) + 1:] if numbers else numbers


# ----------------------------
def remove_smallest(numbers):
    return [n for i, n in enumerate(numbers) if i != numbers.index(min(numbers))]


# ---------------------------
def remove_smallest(numbers):
    return [numbers[i] for i in range(len(numbers)) if i != numbers.index(min(numbers))]


# ---------------------------
# OOD OR EVEN
def oddOrEven(arr):
    return 'odd' if sum(i for i in arr) % 2 else 'even'


# ----------------
def oddOrEven(arr):
    return "odd" if sum(arr) % 2 else "even"


# ------------------
def oddOrEven(arr):
    return ('even', 'odd')[sum(arr) % 2]


# -----------------
def oddOrEven(arr):
    return {True: 'odd', False: 'even'}[sum(arr) % 2]


# -----------------
def filter_list(l):
    return [i for i in l if type(i) == int]


# ---------------------
def filter_list(l):
    return [e for e in l if isinstance(e, int)]


# ----------------------
def filter_list(l):
    return filter(lambda x: isinstance(x, int), l)


# -----------------------
def filter_list(l):
    return list(filter(lambda x: type(x) == int, l))


# -------------------------
# Descending_Order
# moje glowne rozwiazanie
# def Descending_Order(num):
#     return int(''.join(reversed(sorted(list(str(num))))))
# --------------------------
# def Descending_Order(num):
#     return int("".join(sorted(str(num), reverse=True)))
# ------------------------
# trening while
def Descending_Order(num):
    new_num = ''
    new_list = list(str(num))
    iter = 0
    while iter < len(str(num)):
        new_num += max(new_list)
        new_list.remove(max(new_list))
        iter += 1
    return int(new_num)


def has_subpattern(string):
    #     print(len(string))
    #     x = 2
    #     len_looping = len(string)
    #     while len_looping > 0:
    #         z = "{}".format(string[:x])
    #         y = "{}".format(string[x*-1:])
    # #         print(z)
    #         if z == y and len(z) != len(string):
    # #             if len(string) % len(z) == 0: # to pod spodem dziala tak samo de facto
    #             a = (re.findall(z, string))
    #             if len(a) * len(z) == len(string):
    #                 return True
    #         x+=1
    #         len_looping -= 1
    #     return False
    #  zle rozwiazanie
    x = 2
    len_looping = len(string) // 2 + 1
    print(len_looping)
    while len_looping > 0:
        z = "({})+".format(string[:x])
        print(z)
        a = re.fullmatch(z, string)
        print(a)
        if a is not None:
            return True
        x += 1
        len_looping -= 1
    return False

# # działającealeniewydajne
# def has_subpattern(string):
#     x = 2
#     len_looping = len(string)//2 + 1
#     while len_looping > 0:
#         z = "({})+".format(string[:x])
#         a = (re.fullmatch(z, string))
#         if len(string) == 1:
#             return False
#         elif len_looping == 2:
#             return False
#         elif len(a) * len(z) == len(string):
#             return True
#         x+=1
#         len_looping -= 1


if __name__ == '__main__':
    # print (solve("f tyy i wlo kw ypg m lxz cnh cwccog sky  "))
    # print (rever("world"))
    # print (draw_stairs(4))
    # print ('{:>{}}'.format("dupa", 20))
    # print (validate_pin('1234'))
    # print(square_digits(9119))
    # print(remove_smallest([5, 2, 1, 4, 1]))
    # print (oddOrEven([1023, 1, 3]))
    # print(filter_list([1,2,'a','b']))
    # print(Descending_Order(1223445))
    print(has_subpattern("a"))
    # print (has_subpattern("vfcLf1S2vO4GU0t0vyjw34fj4Z0IxyWNinI4HKZedGPDzHJg7NTl6xDx2inqS"))

# https://www.geeksforgeeks.org/sudoku-backtracking-7/
